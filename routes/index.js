var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Social app' });
});

router.get('/api/posts', function (req, res) {
   res.json([
       {
            username: 'robsouth',
            body: 'node is decent'
       }
   ]) 
});

router.post('/api/posts', function (req, res) {
    console.log("post received");
    console.log(req.body.username);
    console.log(req.body.body);
    res.sendStatus(201);
});

module.exports = router;
